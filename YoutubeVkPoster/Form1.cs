﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using System.IO;
using System.Net;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using VkNet;
using System.Threading;

namespace YoutubeVkPoster
{
    public partial class Form1 : Form
    {
        YouTubeService youtubeService;
        VkApi vk;
        public Form1()
        {
            vk = new VkApi();
            youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                ApiKey = "AIzaSyBdf1AIk_62UByFzUvOdnphcO4PM9ywJ9U",
                ApplicationName = this.GetType().ToString()
            });
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (this.Size.Width != 618)
            {
                this.Size = new Size(618, this.Size.Height);
                button3.Text = "<";
            }

            else
            {
                this.Size = new Size(216, this.Size.Height);
                button3.Text = ">";
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            vk.Authorize(new ApiAuthParams
            {
                ApplicationId = 5760538,
                Login = textBox5.Text,
                Password = textBox6.Text,
                Settings = VkNet.Enums.Filters.Settings.All
            });
            if (vk.IsAuthorized)
                textBox4.AppendText(DateTime.Now.ToString("HH:mm:ss") + " Авторизация прошла успешно\n");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<string> data = new List<string>();
            data.Add(textBox5.Text);
            data.Add(textBox6.Text);
            data.Add(textBox1.Text);
            data.Add(textBox2.Text);
            data.Add(textBox3.Text);
            File.WriteAllLines("data", data);
            try
            {
                timer1.Interval = Convert.ToInt32(textBox3.Text);
            }
            catch (Exception)
            {
                timer1.Interval = 500;
            }
            timer1.Enabled = true;
            textBox4.AppendText(DateTime.Now.ToString("HH:mm:ss") + " Начинаю работу с интервалом " + textBox3.Text + "мс\n");
        }

        private async Task<List<string>> getVideoIds(string channel)
        {
            List<string> videos = new List<string>();
            var req = youtubeService.Channels.List("contentDetails");
            req.Id = channel;
            var searchListResponse = await req.ExecuteAsync();
            var req2 = youtubeService.PlaylistItems.List("snippet");
            req2.PlaylistId = searchListResponse.Items[0].ContentDetails.RelatedPlaylists.Uploads;
            req2.MaxResults = 20;
            var searchVideosResponse = await req2.ExecuteAsync();
            foreach (var item in searchVideosResponse.Items)
            {
                videos.Add(item.Snippet.ResourceId.VideoId);
            }
            return videos;
        }

        private void postToVk(int group, string id)
        {
            var req = vk.Video.Save(new VkNet.Model.RequestParams.VideoSaveParams
            {
                Wallpost = true,
                Link = "https://www.youtube.com/watch?v=" + id,
                GroupId = group
            });
            WebRequest req2 = WebRequest.Create(req.UploadUrl);
            WebResponse resp = req2.GetResponse();
            textBox4.AppendText(DateTime.Now.ToString("HH:mm:ss") + " Опубликовано новое видео" + "\n");
            textBox4.AppendText("https://www.youtube.com/watch?v=" + id + "\n");
        }

        private async void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            textBox4.AppendText(DateTime.Now.ToString("HH:mm:ss") + " Проверяю канал\n");
            var vd = await getVideoIds(textBox1.Text);
            IEnumerable<string> VdForPost = Enumerable.Empty<string>();
            if (!File.Exists("history"))
            {
                File.WriteAllLines("history", vd);
                textBox4.AppendText(DateTime.Now.ToString("HH:mm:ss") + " Файл истории создан\n");
            }
            else
            {
                List<string> last = new List<string>(File.ReadAllLines("history"));
                VdForPost = vd.Except(last);
                textBox4.AppendText(DateTime.Now.ToString("HH:mm:ss") + " Новых видео " + VdForPost.Count().ToString() + "\n");
                foreach (var i in VdForPost)
                {
                    var thread = new Thread(
    () =>
    {
        MessageBox.Show("Добавлено новое видео");
    });
                    thread.Start();
                    postToVk(Convert.ToInt32(textBox2.Text), i);
                    await Task.Delay(300);
                }
                File.WriteAllLines("history", vd);
                textBox4.AppendText(DateTime.Now.ToString("HH:mm:ss") + " Файл истории обновлен\n");
            }
            timer1.Enabled = true;
        }

        private void textBox5_Enter(object sender, EventArgs e)
        {
            if (textBox5.Text == "Логин")
            {
                textBox5.Text = "";
                textBox5.ForeColor = Color.Black;
            }

        }

        private void textBox5_Leave(object sender, EventArgs e)
        {
            if (textBox5.Text == "")
            {
                textBox5.Text = "Логин";
                textBox5.ForeColor = Color.Gray;
            }
        }

        private void textBox6_Enter(object sender, EventArgs e)
        {
            if (textBox6.Text == "Пароль")
            {
                textBox6.Text = "";
                textBox6.ForeColor = Color.Black;
                textBox6.PasswordChar = '*';
            }
        }

        private void textBox6_Leave(object sender, EventArgs e)
        {
            if (textBox6.Text == "")
            {
                textBox6.Text = "Пароль";
                textBox6.ForeColor = Color.Gray;
                textBox6.PasswordChar = '\0';
            }
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "ID канала youtube")
            {
                textBox1.Text = "";
                textBox1.ForeColor = Color.Black;
            }
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox1.Text = "ID канала youtube";
                textBox1.ForeColor = Color.Gray;
            }
        }

        private void textBox2_Enter(object sender, EventArgs e)
        {
            if (textBox2.Text == "ID группы ВК")
            {
                textBox2.Text = "";
                textBox2.ForeColor = Color.Black;
            }
        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
            {
                textBox2.Text = "ID группы ВК";
                textBox2.ForeColor = Color.Gray;
            }
        }

        private void textBox3_Enter(object sender, EventArgs e)
        {
            if (textBox3.Text == "Задержка в мс")
            {
                textBox3.Text = "";
                textBox3.ForeColor = Color.Black;
            }
        }

        private void textBox3_Leave(object sender, EventArgs e)
        {
            if (textBox3.Text == "")
            {
                textBox3.Text = "Задержка в мс";
                textBox3.ForeColor = Color.Gray;
            }
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            this.MaximizeBox = false;
            if (File.Exists("data"))
            {
                List<string> data = new List<string>(File.ReadAllLines("data"));
                textBox5.Text = data[0];
                textBox6.Text = data[1];
                textBox1.Text = data[2];
                textBox2.Text = data[3];
                textBox3.Text = data[4];
                button1_Click(sender, e);
                await Task.Delay(500);
                button2_Click(sender, e);   
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Hide();
                notifyIcon1.Visible = true;
            }
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            notifyIcon1.Visible = true;
            this.Hide();
        }
    }
}
